#!/bin/bash

iterations=$1
enc=$2
toWave=$3
extension=$4
testname=$5

if \
	[[ "$iterations" == "" ]] || \
	[[ "$iterations" -lt "1" ]] || \
	[[ ! -f "$enc" ]] || \
	[[ ! -f "$toWave" ]] || \
	[[ "$extension" == "" ]] || \
	[[ "$testname" == "" ]] ; then
	echo "Args: <iterations> <encoder> <wav decoder> <extension> <test name>"
	exit 1
fi

sample=sample-44k1-s16le-2ch.wav
log="$testname/encoding.log"

function log
{
	echo "$@" | tee -a "$log"
}


mkdir -p "$testname"
fLast="$sample"

for i in $( seq $iterations ) ; do
	current="$testname/$(printf "%04d" $i).$extension"

	if [[ -f "$current" ]] ; then
		log "Cached $i of $iterations in '$current'"
	else
		log "Encoding $i of $iterations to '$current'"
		
		tmp1="$testname/tmp-1.wav"
		tmp2="$testname/tmp-2.wav"

		cat "$fLast" | ./$toWave > "$tmp1" 2>> "$log"
		# Note official doc is wrong, debian man page is correct
		# http://manpages.ubuntu.com/manpages/xenial/man1/sox.1.html
		sox "$tmp1" "$tmp2" norm -3 &>> "$log"
		cat "$tmp2" | ./$enc > $current 2>> "$log"

		rm "$tmp1" "$tmp2" || exit 1
	fi

	fLast="$current"
done


