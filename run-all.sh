#!/bin/bash

run=run-test.sh

function run_one
{
	local encoder=$1
	local extension=$2
	local test_name=$3
	
	local i=100
	
	./run-test.sh $i $encoder to-wave-s16.sh $extension ${test_name}-s16
	./run-test.sh $i $encoder to-wave-f32.sh $extension ${test_name}-f32
	./run-test.sh $i $encoder to-wave-f64.sh $extension ${test_name}-f64
}

run_one  encode-aac-128.sh    aac aac-q4
run_one  encode-ac3-256.sh    ac3 ac3-256k
run_one  encode-dts-256.sh    dts dts-256k
run_one  encode-mp3-128.sh    mp3 mp3-128k
run_one  encode-mp3-320.sh    mp3 mp3-320k
run_one  encode-mp3-q5.sh     mp3 mp3-q5
run_one  encode-vorbis-128.sh ogg vorbis-128k
run_one  encode-vorbis-q4.sh  ogg vorbis-q4
