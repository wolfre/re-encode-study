# re-encode study

A study on how re-encoding audio using the same codec parameters deteriorates quality over time.
I just became curious, not sure how useful this is.

# running it

You will need a sample to test against (not included in this repo).
This sample should have some properties:

- uncompressed source (probably from some audio cd)
- about 30 to 60 sec in length
- should have a good recording quality
- should have a good selection of instruments
  - few percussion instruments
  - drums
  - strings or piano
- sample must be a wave with
  - sample rate 44100
  - stereo
  - signed 16 bit little endian (MS Wave)
- name the sample **sample-44k1-s16le-2ch.wav**

You'll need
- bash
- ffmpeg
- sox

Run
```bash
./run-all.sh
```

Each codec configuration will be tested 3 times. 
The idea is to find out how quantization plays into the quality aspect.
Each time with a different intermediary format:
- **s16** signed 16bit little endian linear pcm (MS wave)
- **f32** 32bit float pcm
- **f64** 64bit float pcm

Each codec will produce 3 folders, containing (100) encoded files, one for each re-encoding.

# Results 

## ubuntu 20.04 / ffmpeg 4.2.2-1ubuntu1 / SoX v14.4.2

Note that all below results are based on my subjective impression of what I hear.
This is not very scientific and is not even close to proper [double-blind tests](https://en.wikipedia.org/wiki/Blinded_experiment).
The results were obtained with code from 2cf0afb5dc735f5a838b2753587853cb41daee3a .
If not noted otherwise the below observations were done using audacity to quickly switch between iterations / codecs / etc.

The following individual sections only concern one codec, and the subjective impressions I had.
For comparison between f32 / f64 / s16, a "random" iteration was chosen that exhibited quite some deterioration, but still had quite a bit of the original audio discernible.
This was done to easier hear differences, and draw better comparisons.
The test script does normalize the output of each re-encode to the same level.
This avoids completely loosing audio (into either silence or distortion), if codecs *tamper* with the volume.
Below notes on comparative volume should therefore be taken more as a sort of "signal to artifact / noise level" than an absolute volume.

The last section will attempt to compare all codecs against each other.

For reference, here's [the output of *ffmpeg -buildconf*](ffmpeg-4.2.2-1ubuntu1-buildconf) used for these tests.

### aac-q4

Observations on encoding 20
- Codec tends to over emphasis attacks (percussion) and dampens other things.
  Over emphasised attacks cause quite high pitched artifacts.
  Sound seems very room-ish as if less direct sound hit the mic.
  Instrument placement on the virtual stage is difficult.
- Looking at f32, f64 and s16 shows a slight difference.
  f32 and f64 tend to emphasis attacks more than s16.
  Also for f32 and f64 the music was quieter than s16 compared to compression artifacts.
- Encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 2200 ms.
  
### ac3-256k

AC3 does not seem to loose quality with re-encoding, encoding 1 sonds the same as 100.
Observations on encoding 100
- No obvious difference between either f32, f64 or s16.
- The encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 570 ms.

### dts-256k

Observations on encoding 20
- Codec tends to *muddy* percussions into a sort of *bubbly* sound (not sure how to better describe that, very similar to low quality mp3).
  Sound seems a bit room-ish as if less direct sound hit the mic.
  Instrument placement on the virtual stage is difficult.
- Looking at f32, f64 and s16 shows a slight difference.
  f32 and f64 seem to retains slightly more information in the percussion sections than s16.
- Encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 1150 ms.

### mp3-128k

Observations on encoding 30
- Codec tends to *muddy* higher frequencies, especially percussions into a sort of *bubbly* sound.
  A very exaggerated *low quality mp3 effect*.
  Sound scape generally does not seem to affected.
- No obvious difference between either f32, f64 or s16.
- Encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 2400 ms.

### mp3-320k

Observations on encoding 100
- Codec tends to *muddy* higher frequencies, especially percussions into a sort of *bubbly* sound.
  A very exaggerated *low quality mp3 effect*.
  Sound scape generally does not seem to affected.
  (Again subjective) sound quality after 100 re-rencodings of 320kbit is about the same as around 20 of 128kbit
- No obvious difference between either f32, f64 or s16.
- Encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 2400 ms.

### mp3-q5

Observations on encoding 30
- Codec tends to *muddy* higher frequencies, especially percussions into a sort of *bubbly* sound.
  A very exaggerated *low quality mp3 effect*.
  Compared to the 128kbit and 320kbit fixed variants this *bubbly* sound introduces almost noise like, distorted high frequency artifacts around the percussions.
  Instrument placement on the virtual stage is negatively affected.
- No obvious difference between either f32, f64 or s16.
- Encoder does add a little silence in the beginning, with each encode.
  At encoding 100 this totals to about 2400 ms.
  
### vorbis-128k

Observations on encoding 30
- Codec tends to over emphasis attacks (percussion) and dampens other things.
  Over emphasised attacks cause distortion artifacts that are some what lower pitched than the *low quality mp3 effect*.
  Sound seems very room-ish as if less direct sound hit the mic.
  Instrument placement on the virtual stage is difficult.
- Looking at f32, f64 and s16 shows a slight difference.
  f32 and f64 tend to emphasis attacks more than s16, but at the same time retain slightly more of the original quality.
  The resulting volume for f32, f64 and s16 is roughly the same.
- Encoder does add a little silence at the end, with each encode.
  At encoding 100 this totals to about 200 ms.

### vorbis-q4

Observations on encoding 30
- identical to [vorbis-128k](#vorbis-128k)

### Versus

This is an attempt to compare the above codecs, subjectively.
Only using s16 samples, I started out with one iteration of one codec: aac-q4 nr. 20.
This iteration did have quite some degradation, but still had enough *music* in it to be comparable.
Next I tried to find an iteration for each codec with roughly the same subjective quality.
Again this is highly subjective and rather hard to do due to the different properties of each codec.
I tried to compare by *how much music* was retained and tried to ignore compression artifacts a bit.
Here are my results:

- aac-q4 **20** (reference, all other entries were compared against this)
- ac3-256k **n/a** - this codec is not impacted by re-encoding
- dts-256k **40**
- mp3-128k **50**
- mp3-320k **n/a** - this high bitrate did sound quite a bit better at iteration 100 than the reference
- mp3-q5 **40**
- vorbis-128k **30**
- vorbis-q4 **30**
